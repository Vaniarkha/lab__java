
import java.util.Scanner;

public class Lab__x001 {

        public static void main(String[] args) {
            // write your code here
            Scanner sc = new Scanner(System.in);
            System.out.print("Введите десятичное число: ");
            int number = sc.nextInt();
            System.out.println("Вы ввели число : " + number);
            sc.close();
            String intBits = Integer.toBinaryString(number);
            System.out.println("2: " + intBits);
            String intB = Integer.toHexString(number);
            System.out.println("16: " + intB);
            String intOct = Integer.toOctalString(number);
            System.out.println("8: " + intOct);

        }
    }


